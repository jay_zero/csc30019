#include "Game.h"

using namespace v1l85;

int main( int argc, char *argv[] ){

	//Where the bulk of the application lies
	Game* g = new Game();

	g->init();
	g->loop();

	g->cleanup();

	delete g;
	return 0;
	
}	
#include "Game.h"
#include "GameStateManager.h"
#include "SplashState.h"

using namespace v1l85;

Game::Game() : windowHnd(0), isQuitting(0){

}

Game::~Game(){
	
}

void Game::init(){

	//Initialise Ogre Settings and create the new Render Window & Scene Manager
	ogreInit();

    //Push a new Splashscreen state to the GSM
	GameStateManager::getSingleton().pushState( new SplashState(this) );

	window->getCustomAttribute("WINDOW", &windowHnd);

	im = OIS::InputManager::createInputSystem(windowHnd);
    keyboard = static_cast<OIS::Keyboard*>(im->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(im->createInputObject(OIS::OISMouse, false));

}

void Game::ogreInit(){

	std::string resourcePath;
    resourcePath = "";
    
    root = new Ogre::Root(resourcePath + "plugins.cfg", resourcePath + "ogre.cfg", "Ogre.log");
    
    if (!root->showConfigDialog())
        exit(-1);

    window = root->initialise(true, "Physics Platform Jumper");
    smgr = root->createSceneManager(Ogre::ST_GENERIC);
    
    Ogre::ConfigFile cf;
    cf.load(resourcePath + "resources.cfg");
    
    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    
    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
    
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    
    //Create a camera
    cam = smgr->createCamera("Camera1");
    // cam->setPosition(Ogre::Vector3(0,10,50));
    // cam->lookAt(Ogre::Vector3(0,0,0));
    cam->setNearClipDistance(5);
    Ogre::Viewport* vp = window->addViewport(cam);
    cam->setAspectRatio(Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualWidth()) );

    //For Debugging particles.
    // cam->setPosition(10,10,10);
    // cam->lookAt(0,0,0);

    //Fog
    Ogre::ColourValue fogColour( 0.42, 0.64, 0.95 );
    smgr->setFog( Ogre::FOG_LINEAR, fogColour, 0.5, 100, 500 );
    vp->setBackgroundColour(fogColour);
    // smgr->setFog(Ogre::FOG_EXP, fogColour, 0.005);


}


void Game::loop(){

     // We will use Ogre's hi-res timer to control the game loop
    Ogre::Timer *timer = new Ogre::Timer();
    
    // Variables for the game loop - see lecture 2
    long currentTime=timer->getMilliseconds(), lastTime=currentTime;
    long frame_length = 20; // milliseconds
    long accumulator = 0;
    int maxUpdatesPerFrame = 10;

	while( true ){

        int updatesMade = 0;

		Ogre::WindowEventUtilities::messagePump();

		keyboard->capture();
        mouse->capture();

		if (keyboard->isKeyDown(OIS::KC_ESCAPE) || isQuitting ) break;

        GameStateManager::getSingleton().handleInput();

        // Work out how much time has elapsed, add to accumulator
        currentTime=timer->getMilliseconds();
        long elapsedTime = currentTime - lastTime;
        accumulator += elapsedTime;
        
        // If we're on a fast PC we can "sleep" for, say, 5ms, allowing other programs time on the CPU
        if( accumulator < frame_length ) { usleep(5000); root->renderOneFrame(); continue; }
        
        // Otherwise, we'll update the game logic and, finally, render the scene
        lastTime = currentTime;
        while( accumulator >= frame_length && updatesMade++ < maxUpdatesPerFrame ) {
            GameStateManager::getSingleton().update( elapsedTime );
            accumulator -= frame_length;
        }

		if( root->renderOneFrame() == false ) break;
	}

}

void Game::cleanup(){
	//Tidy up after executing ;D
	GameStateManager::getSingleton().cleanup();
	delete root;
}
# Simple makefile for compilation of games computing coursework
# Ref: http://www.opussoftware.com/tutorial/TutMakefile.htm

# Lab Paths
OGRE_PATH=/users/students/l/v1/v1l85/ogre_sdk/
ODE_PATH=/users/students/l/v1/v1l85/ode-0.13.1/

# Home Paths
#OGRE_PATH=/home/jamie/ogre_sdk/
#ODE_PATH=/home/jamie/ode-0.13/

CC=g++
CFLAGS=-c -Wall -I $(OGRE_PATH)include/OGRE -I $(ODE_PATH)include
LDFLAGS=-L $(ODE_PATH)ode/src/.libs -L $(OGRE_PATH)lib -lOgreMain -lfreetype -lGL -lX11 -ldl  -lXt -lzzip -lOIS -lXaw -lode -lpthread
SOURCES=main.cpp GameStateManager.cpp SplashState.cpp Game.cpp Platform.cpp PlayState.cpp Player.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=game

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *o

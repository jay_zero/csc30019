#ifndef PLAYER_H_
#define PLAYER_H_

#include "GameObject.h"
#include "Ogre.h"

using namespace Ogre;

namespace v1l85{

	class Player : public GameObject{ 

		public:

			Player() : isMoving(false) {};
			Player( Ogre::SceneManager* smgr, dWorldID world, dSpaceID space );

			virtual void update();
			virtual void draw();
			void startMoving();

			dBodyID getBody(){
				return body;
			}

			const dReal* getPosition(){
				return dBodyGetPosition( body );
			}
		
		private:
			dBodyID body;
			bool isMoving;
			Ogre::RibbonTrail* trail;
	};

}

#endif

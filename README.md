# CSC 30019 Assignment
A simple game using the OGRE / ODE frameworks.  The objective of the game is to land the player on each of the sequential platforms.  Hitting a platform will propel you to the next platform and score a point.  In this simple version, there are 100 platforms, so a maximum score of 100 (for the time being).

###Build

Included is a Unix Makefile to build the project although paths to OGRE / ODE libraries should be updated.  In the Makefile, update the following lines to reflect the relevant directories on your machine:

```
OGRE_PATH=/path/to/ogre_sdk/
ODE_PATH=/path/to/ode-0.13/
```

Additionally, you will need to update the `plugins.cfg` with the path to the Ogre SDK plugins.

```
PluginFolder=/path/to/ogre_sdk/lib/OGRE
```

Once the make file has been updated, compile the program by running `make`.  And run the game by running `./game`.

###Controls

The game is very simple.  From anywhere in the game pressing `Escape` will quit from the game.

Pressing `Return` in the title screen will take you to the game's play state.

Whilst playing the game, try to move the character using `A` and `D` to land on the platforms.

###Gameplay Demo

A video demonstrating gameplay (before a couple of changes to the camera etc.) can be viewed online below.

[![Gameplay Demo](http://img.youtube.com/vi/4faHK-oEmfs/0.jpg)](http://www.youtube.com/watch?v=4faHK-oEmfs)

---

Author: [Jamie Watson](http://jamiewatson.me)
Date: November / December 2014

#include "Player.h"


using namespace v1l85;


Player::Player( Ogre::SceneManager* smgr, dWorldID world, dSpaceID space ) : isMoving(false){

	dMass m;

	body = dBodyCreate(world);
	dBodySetPosition( body, 0, 10, 0);
	dMassSetBox( &m,1,2,2,2);
	dMassAdjust( &m, 5);
	dBodySetMass( body, &m);
	objGeom = dCreateBox( space, 2,2,2);
	dGeomSetBody(objGeom, body );

	// dBodySetLinearVel( body, 0, 0, -0.4f);


	Ogre::Entity* ent = smgr->createEntity("ogrehead.mesh");
	objNode = smgr->getRootSceneNode()->createChildSceneNode();
	objNode->attachObject(ent);
	objNode->scale(0.02,0.02,0.02);


	NameValuePairList params;
	params["numberOfChains"] = "2";
	params["maxElements"] = "80";
	trail = (RibbonTrail*)smgr->createMovableObject("RibbonTrail", &params);
	trail->setMaterialName("Examples/LightRibbonTrail");
	trail->setTrailLength(20);
	trail->setVisible(true);
	smgr->getRootSceneNode()->attachObject(trail);


	for (int i = 0; i < 2; i++)
	{
		trail->setInitialColour(i, 1, 0.8, 0);
		trail->setColourChange(i, 0.75, 1.25, 1.25, 1.25);
		trail->setWidthChange(i, 1);
		trail->setInitialWidth(i, 0.5);
	}

	trail->setVisible(true);
	trail->addNode(objNode);

}


void Player::update(){

	const dReal *pos = getPosition();
	objNode->setPosition(pos[0],pos[1],pos[2]);

}

void Player::draw(){}

void Player::startMoving(){

	if( !isMoving ){
		isMoving = true;
		const dReal* v = dBodyGetLinearVel( getBody() );
		dBodySetLinearVel( body, v[0], v[1], 4.9f);
	}

}

#ifndef PLAY_STATE_H
#define PLAY_STATE_H

#include <ode/ode.h>
#include "GameState.h"
#include "Game.h"
#include "Platform.h"
#include "Player.h"
#include <vector>
#include "OgreText.h"

namespace v1l85{

	class PlayState : public GameState{

		private:
			int score;
			int highScore;
			
			dWorldID world;
			dJointGroupID contactgroup;
			dSpaceID space;
			Platform p;
			std::vector<Platform> platforms; //Platforms extend GameObject, so GameObject could be used but in this implementation there is no power ups or anything.
			Player player;
			OgreText *textScore;
			OgreText *textBest;
			
		public:
			virtual void cleanup();
			virtual void init();
			virtual void update( long delta );
			virtual void handleInput();

			void initPhysics();
			void updatePhysics();
			void nearCallback( dGeomID o1, dGeomID o2 );

			void getHighScore();
			void saveHighScore( int score );

			PlayState( Game* g ) : GameState( g ){ }

	};
	
}

#endif
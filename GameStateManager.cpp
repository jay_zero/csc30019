#include "GameStateManager.h"
#include <iostream>
using namespace std;
using namespace v1l85;


/*!
 * Push GameState to the back of the std::vector, states.
 */

void GameStateManager::pushState( GameState* state ){
	state->init();
	states.push_back( state );
}

/*!
 * 
 */

void GameStateManager::popState(){
	states.back()->cleanup();
	states.pop_back();
}

/*!
 * Remove the last item from the vector and replace it with the new GameState
 */


void GameStateManager::changeState( GameState* state ){
	if( !states.empty() ){
		states.back()->cleanup();
		states.pop_back();
	}

	pushState( state );
}


/*!
 * Update the current Game State
 */

 void GameStateManager::update( long delta ){
 	states.back()->update( delta );
 }

  void GameStateManager::handleInput(){
 	states.back()->handleInput();
 }

 /*!
 * Clean Up after the GSM
 */

 void GameStateManager::cleanup(){
 	while( !states.empty() ){
 		states.back()->cleanup();
 		states.pop_back();
 	}
 }
#ifndef GAME_H_
#define GAME_H_

// OGRE headers
#include "Ogre.h"
#include "OgrePlugin.h"
#include "OgreVector3.h"
#include <OIS/OIS.h>

namespace v1l85{
	
	class Game{

		public:
			Ogre::Root* root;
			Ogre::RenderWindow* window;
			Ogre::SceneManager* smgr;
			Ogre::Camera* cam;

			Game();
			~Game();

			void init();
			void loop();
			void cleanup();
			void quit(){ isQuitting = true; }

			OIS::Keyboard* getKeyboard(){ return keyboard; }
			OIS::Mouse* getMouse(){ return mouse; }
			OIS::InputManager* getInputManager(){ return im; }

			Ogre::RenderWindow* getWindow(){ return window; }
			Ogre::SceneManager* getSceneManager(){ return smgr; }
			Ogre::Root* getRoot(){ return root; }

			Ogre::Camera* getCamera(){ return cam; }
			void setCamera( Ogre::Camera* cam){ this->cam = cam; }

		private:
			void ogreInit();
			OIS::Keyboard* keyboard;
			OIS::InputManager* im;
			OIS::Mouse* mouse;
			size_t windowHnd;
			bool isQuitting;

	};

}

#endif
#include "PlayState.h"
#include "SplashState.h"
#include "GameStateManager.h"
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>   
#include <OIS/OIS.h>
#include "OgreMaterialManager.h"
#include "OgreOverlayManager.h"
#include "OgreTechnique.h"
#include "OgreBlendMode.h"
#include "OgreTextAreaOverlayElement.h"

using namespace v1l85;
using namespace std;

int OgreText::init=0;

// Global function to call the Physics callback in the Play State

void gNearCallback (void *data, dGeomID o1, dGeomID o2 )
{
    PlayState* d = (PlayState*) data;
    assert( d != NULL );

    d->nearCallback( o1, o2 );
}

void PlayState::cleanup(){
	cout << "Cleaning up after the game play" << endl;

    if( score > highScore )
        saveHighScore( score );

    // game->getSceneManager()->clearScene();
    Ogre::SceneManager* smgr = game->getSceneManager();
    smgr->destroyAllLights();
    smgr->destroyAllEntities();
    smgr->destroyAllRibbonTrails();
    smgr->destroyAllParticleSystems();

    delete textScore;
    delete textBest;

	dSpaceDestroy (space);
    dWorldDestroy (world);
    dCloseODE();
}

void PlayState::init(){
	cout << "Initialising the play state and game world" << endl;

	//Grab the Scene Manager
	Ogre::SceneManager* smgr = game->getSceneManager();

	smgr->setAmbientLight(Ogre::ColourValue(0.5,0.5,0.5));


	Ogre:: Light *dirLight = smgr->createLight("dir_light");
    dirLight->setType(Ogre::Light::LT_DIRECTIONAL );
    dirLight->setDirection(Ogre::Vector3(0.2,-1.0,0));
    dirLight->setDiffuseColour(0.69f,0.75f,0.94f);

    smgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    smgr->setSkyBox(true, "v1l85/CustomSkyBox", 10);

    //Initialise the ODE physics.  I should really move this to a "Physics Manager"
    initPhysics();

    score = 0;

    platforms.push_back( Platform(smgr,space, 0, 0, 0,1.0f, 1.0f, 1.0f) );
    platforms.back().bMoveable = false; // Never allow the first platform to be a moveable box.


    //In this implementation, just create 100 platforms as a sort of "proof of concept"
    for( int x = 1; x < 100; x++){
        // cout << "Creating platform " << x << endl; 
        platforms.push_back( Platform(smgr,space, rand() % 30 - 15, 0, x*20,1.0f, 1.0f, 1.0f) );
    }

    // Create the player then render one frame
    player = Player( smgr, world, space );

    textScore=new OgreText;                            
    textScore->setPos(0.01f,0.01f);        
    textScore->setCol(0,0,0,1);    
    textScore->setCharHeight(0.06f);


    getHighScore();

    textBest=new OgreText;                            
  
    textBest->setPos(0.01f,0.07f);        
    textBest->setCol(0,0,0,1);    
    textBest->setCharHeight(0.04f);

    game->getRoot()->renderOneFrame(); 
}

void PlayState::update( long delta ){

    // Not C++11 unfortunately...
    std::stringstream s;
    s << "Score: " << score;
    textScore->setText(s.str()); 

    std::stringstream h;
    h << "Best: " << highScore;
    textBest->setText(h.str()); 


	updatePhysics();
    player.update();

    //Update all of the platforms in the scene
    for(std::vector<Platform>::iterator it = platforms.begin(); it != platforms.end(); ++it) {
        it->update ( delta );
    }

    Ogre::Vector3 pos = player.getSceneNode()->getPosition();

    //Follow the player unless we miss a platform

    if( pos.y > -10.0f ){
        game->getCamera()->setPosition(pos.x,pos.y+10,pos.z-10);
    }

    //Game Over
    if( pos.y < -100.0f ){
        GameStateManager::getSingleton().changeState( new SplashState(game) );
    }

    game->getCamera()->lookAt(Ogre::Vector3(pos.x,pos.y-2, pos.z+2));

}

void PlayState::handleInput(){

    //Should be in the player controller really.
    const dReal* v = dBodyGetLinearVel( player.getBody() );

    if (game->getKeyboard()->isKeyDown(OIS::KC_A) ){
        dBodySetLinearVel( player.getBody(), 7.0f,v[1],v[2]);
        player.getSceneNode()->setOrientation(Ogre::Quaternion(Ogre::Degree(45), Ogre::Vector3(0,1,0)));
    }else if (game->getKeyboard()->isKeyDown(OIS::KC_D) ){
        dBodySetLinearVel( player.getBody(), -7.0f,v[1],v[2]);
        player.getSceneNode()->setOrientation(Ogre::Quaternion(Ogre::Degree(-45), Ogre::Vector3(0,1,0)));
    }else{
        dBodySetLinearVel( player.getBody(), 0.0f,v[1],v[2]);
        player.getSceneNode()->setOrientation(Ogre::Quaternion(Ogre::Degree(0), Ogre::Vector3(0,1,0)));
    }
  

}

void PlayState::initPhysics(){

	dInitODE2(0);
    world = dWorldCreate();
    space = dHashSpaceCreate (0);
    contactgroup = dJointGroupCreate (0);
    dWorldSetGravity (world,0,-9.8,0);

}


void PlayState::nearCallback( dGeomID o1, dGeomID o2 ){

    dContact contact[10];

    if( o2 == player.getObjectGeom() || o1 == player.getObjectGeom() ){
        if( dCollide( o1, o2, 10, &contact[0].geom, sizeof(dContact)) > 0 ) {

            const dReal* v = dBodyGetLinearVel( player.getBody() );

            dBodySetLinearVel( player.getBody(), v[0],0,v[2]);
            dBodyAddForce( player.getBody(), 0, 2000, 0 );

            //Start the player moving forwards, this is just to make sure the player hits a platform to start
            player.startMoving();


            //This is bad but... just a quick check to find the current platform so that we only score once.
            //Trouble is, the more platforms, the longer this search will take -_-
            for(std::vector<Platform>::iterator it = platforms.begin(); it != platforms.end(); ++it) {
                if( (o1 == it->getObjectGeom() || o2 == it->getObjectGeom()) && it->bCanScore ){

                    game->getSceneManager()->destroyAllParticleSystems();

                    const dReal* pos = player.getPosition();

                    // cout << pos[0] << pos[1] << pos[2] << endl;

                    // game->getSceneManager()->createChildSceneNode(Vector3(pos[0], pos[1], pos[3]));

                    SceneNode* node = game->getSceneManager()->getRootSceneNode()->createChildSceneNode();
                    node->setPosition( pos[0], pos[1], pos[2] );

                    ParticleSystem* ps;
                    ps = game->getSceneManager()->createParticleSystem("Stars", "v1l85/Stars");
                    ps->setVisible(true);
                    node->attachObject(ps);

                    //CREATE PARTICLE EFFECTS / INCREMENT SCORE / PLAY A SOUND / DO WHATEVER
                    cout << "Score: " << ++score << endl;  

                     

                    it->bCanScore = false; //Stop us from dealing with the collision multiple times creating a bigger score etc.
                }

            }

            return;
        }
    }
    
}

void PlayState::updatePhysics(){

    //Call the global NearCallback function so we can call this->nearCallback
    dSpaceCollide(space, this, &gNearCallback);
    dWorldStep(world, 1.0/20.0);
    dJointGroupEmpty( contactgroup );


}



void PlayState::getHighScore(){
    ifstream in;
    in.open("score");
    if( in.fail() ) highScore = 0;

    in >> highScore;

    in.close();
}

void PlayState::saveHighScore( int score ){
    //Primitive score saving mechanism.
    ofstream out;
    out.open("score");

    out << score;

    out.close();

}
#ifndef PLATFORM_H
#define PLATFORM_H

#include "GameObject.h"
#include "Ogre.h"

namespace v1l85{
	

	class Platform : public GameObject{

		public:

			Platform() : bCanScore(true), bSprung(false){};
			Platform( 
				Ogre::SceneManager* smgr, dSpaceID space, 
				dReal x, dReal y, dReal z,
				dReal width, dReal height, dReal depth );

			virtual void update(){ update(0); };
			virtual void update( long delta = 0 );
			virtual void draw();

			bool bCanScore;
			bool bSprung;
			bool bMoveable;
			int direction;
			Ogre::AnimationState* mAnimationState;
			Ogre::Entity *ent;

	};

}

#endif
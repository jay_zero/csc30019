#ifndef GAME_STATE_H_
#define GAME_STATE_H_

#include "Game.h"

namespace v1l85{

	class GameState{
		public:

			//pure Virtual functions make this an abstract class.  We don't want this instantiated.
			virtual void cleanup() = 0;
			virtual void init() = 0;
			virtual void update( long delta ) = 0;
			virtual void handleInput() = 0;

			GameState( Game* g ){ this->game = g; }

			Game* game;
	};

}

#endif
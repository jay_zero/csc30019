#ifndef SPLASH_STATE_H_
#define SPLASH_STATE_H_

#include "GameState.h"
#include "Game.h"

namespace v1l85{

	class SplashState : public GameState{

		public:
			virtual void cleanup();
			virtual void init();
			virtual void update( long delta );
			virtual void handleInput();

			SplashState( Game* g ) : GameState( g ) { }

		private:
			Ogre::Overlay* overlay;

	};
	
}

#endif
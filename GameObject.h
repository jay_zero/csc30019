#ifndef GAMEOBJ_H
#define GAMEOBJ_H

#include <ode/ode.h>

// OGRE headers
#include "Ogre.h"
#include "OgrePlugin.h"
#include "OgreVector3.h"

namespace v1l85{

	/*!
	 * Abstract GameObject class.  All objects in the world should extend this class.
	 */

	class GameObject{

		protected:
			Ogre::Entity* mesh;
			Ogre::SceneNode* objNode;
			dGeomID objGeom;

		public:
			virtual void update()=0;
			virtual void draw()=0;

			Ogre::SceneNode* getSceneNode(){
				return objNode;
			}

			dGeomID getObjectGeom(){
				return objGeom;
			}			

	};
	
}

#endif
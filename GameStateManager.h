#ifndef GSM_H_
#define GSM_H_

#include "GameState.h"
#include <vector>


namespace v1l85{

	/*!
	 * GameStateManager, Singleton class which manages the state of the game
	 *
	 * Implements a simple game state manager using std::vector.  Using the vector allows us to
	 * push multiple states, in effect we could create a layered "game state" functionality to create
	 * pause menus, option menus which "pause" previous states.  Although there are problems
	 * with the singleton design pattern, it makes sense that there is only ever one of these
	 * within the game.
	 *
	 * There is plenty of literature about game state management available.  Googling the topic
	 * produces relevant links such as http://gamedevgeek.com/tutorials/managing-game-states-in-c/ which is a
	 * common & similar implementation as I have created here.  
	 * 
	 */

	class GameStateManager{
		public:
			static GameStateManager& getSingleton(){
				static GameStateManager gsm;
				return gsm;
			}
			void pushState( GameState* state );
			void changeState( GameState* state );
			void popState();
			void update( long delta );
			void handleInput();
			void cleanup();
		private:
			GameStateManager(){};
			GameStateManager( GameStateManager const&);
			void operator=(GameStateManager const&);

			std::vector<GameState*> states;
	};
}

#endif
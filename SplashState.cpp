#include "SplashState.h"
#include "PlayState.h"
#include "GameStateManager.h"
#include <iostream>
#include <OIS/OIS.h>
#include "OgreMaterialManager.h"
#include "OgreOverlayManager.h"
#include "OgreTechnique.h"
#include "OgreBlendMode.h"

using namespace v1l85;
using namespace std;

void SplashState::cleanup(){
	cout << "Cleaning up after the splash screen" << endl;
	overlay->hide();

	Ogre::OverlayManager::getSingleton().destroyOverlayElement("SplashPanel");
	Ogre::OverlayManager::getSingleton().destroy("SplashOverlay");
}

void SplashState::init(){
	cout << "Initialising the Splash Screen / Loading Screen game state" << endl;

	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();

	Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(overlayManager.createOverlayElement("Panel", "SplashPanel"));
	panel->setMetricsMode(Ogre::GMM_RELATIVE);
	panel->setPosition(0, 0);
	panel->setDimensions(1, 1);
	panel->setMaterialName("v1l85/splash");

	overlay = overlayManager.create("SplashOverlay");
	overlay->add2D(panel);
	overlay->show();
}

void SplashState::update( long delta ){
	if( game->getKeyboard()->isKeyDown(OIS::KC_RETURN) ){
		GameStateManager::getSingleton().changeState( new PlayState(game) );
	}
}

void SplashState::handleInput(){
	// Do Nothing.
}
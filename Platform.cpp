#include "Platform.h"

using namespace v1l85;

Platform::Platform( Ogre::SceneManager* smgr, dSpaceID space, dReal x, dReal y, dReal z, dReal width, dReal height, dReal depth ) : bCanScore(true){

	objNode = smgr->getRootSceneNode()->createChildSceneNode();

	//Probably would be better to not create an entity per platform rather, reuse entity smgr->getEntity("platform")
	ent = smgr->createEntity("Cube.mesh");
	objNode->attachObject(ent);
    objNode->scale(1,1,1);
    // objNode->rotate(Ogre::Quaternion(Ogre::Degree(45), Ogre::Vector3(1,0,0)));

    mAnimationState = ent->getAnimationState("KeyAction");
    

	objGeom = dCreateBox( space, width, height, depth );

    dGeomSetPosition (objGeom,x,y,z);

    // Just to spice the game up a little bit, occasionally create a platform that can move
    if( rand() % 100 > 90 ){
    	bMoveable = true;

    	//uhh.. xD
    	if( rand() % 100 > 50 )
    		direction = -1;
    	else direction = 1;
    }else bMoveable = false;

}

void Platform::update( long delta ){

    const dReal* pos = dGeomGetPosition(objGeom);
    dReal x, y;

    x = pos[0];

    if( bMoveable && bCanScore ){

    	if( pos[0] <= -15 || pos[0]  >= 15)
			direction *= -1;

        x = pos[0] + (direction*0.14);
		
	}

    if( !bCanScore && !bSprung ){
        mAnimationState = ent->getAnimationState("KeyAction.001");
        mAnimationState->setLoop(false);
        mAnimationState->setEnabled(true);
        bSprung = true;
    }

    // if( !bCanScore ){
    //     y = pos[1] - 0.1;
    // }


    dGeomSetPosition( objGeom, x, y, pos[2] );

	// const dReal *gbpos = dGeomGetPosition(objGeom);
 //    dReal gbrot[4];  dGeomGetQuaternion(objGeom,gbrot);
    
    objNode->setPosition(x,y,pos[2]);
    // objNode->setOrientation( gbrot[0], gbrot[1], gbrot[2], gbrot[3]);

    mAnimationState->addTime(1);
	
}

void Platform::draw(){
	
}